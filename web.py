from http.server import BaseHTTPRequestHandler
from http.server import HTTPServer
import re
import rs232

params={"s1":"0", "s2":"0", "s3":"0", "s4":"0", "t1":"0", "t2":"0",
        "s1min":"0", "s2min":"0", "s3min":"0", "s4min":"0", "t1min":"0", "t2min":"0",
        "s1max":"180", "s2max":"180", "s3max":"180", "s4max":"180", "t1max":"255", "t2max":"255"}

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
  server_address = ('', 8000)
  httpd = server_class(server_address, handler_class)
  try:
      httpd.serve_forever()
  except KeyboardInterrupt:
      httpd.server_close()

class HttpGetHandler(BaseHTTPRequestHandler):
    """Обработчик с реализованным методом do_GET."""

    def do_GET(self):
        global params

        path = self.path

        self.send_response(200)
        if path == "/style.css":
            self.send_header("Content-type", "text/css")
        else:
            self.send_header("Content-type", "text/html")
        self.end_headers()

        path = self.path
        if path == "/":
            path = "/index"
#            path = "/index.html"

        try:
            if path == "/style.css":
                file  = open("web"+path, 'r')
            else:
                file  = open("web"+path + ".html", 'r')
        except FileNotFoundError:
            file  = open("web/404.html", 'r')

        message = file.read()
        file.close()
#        self.wfile.write(bytes(message, "utf8"))
        if path == "/support":
            message = message.format(s1=params["s1"], s2=params["s2"], s3=params["s3"], s4=params["s4"], 
                                    s1min=params["s1min"], s2min=params["s2min"], s3min=params["s3min"], s4min=params["s4min"],
                                    s1max=params["s1max"], s2max=params["s2max"], s3max=params["s3max"], s4max=params["s4max"],
                                    t1=params["t1"], t1min=params["t1min"], t1max=params["t1max"],
                                    t2=params["t2"], t2min=params["t2min"], t2max=params["t2max"])
        self.wfile.write(message.encode())
        return

#        self.wfile.write('<html><head><meta charset="utf-8">'.encode())
#        self.wfile.write('<title>Простой HTTP-сервер.</title></head>'.encode())
#        self.wfile.write('<body>Был получен GET-запрос.</body></html>'.encode())

    def do_POST(self):
        global params

        self.send_response(200)
        self.send_header('Location','/support')
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        path = self.path
        #Обработчик подписки
        if path == "/support":
            content_len = int(self.headers.get('Content-Length'))
            post = self.rfile.read(content_len)
#            print(post)
            post = re.sub(r"b'","",str(post))
            post = re.sub(r"'","",post)
            rng = re.split(r"&",str(post))
            print(rng)
            try:
                rs232.sendToSerial(rng[0])
            except:
                print("Send rs232 message error")

            file  = open("web/support.html", 'r')
            message = file.read()
            file.close()
            params[rng[0][0:2]]=rng[0][3:]
            message = message.format(s1=params["s1"], s2=params["s2"], s3=params["s3"], s4=params["s4"], 
                                    s1min=params["s1min"], s2min=params["s2min"], s3min=params["s3min"], s4min=params["s4min"],
                                    s1max=params["s1max"], s2max=params["s2max"], s3max=params["s3max"], s4max=params["s4max"],
                                    t1=params["t1"], t1min=params["t1min"], t1max=params["t1max"],
                                    t2=params["t2"], t2min=params["t2min"], t2max=params["t2max"])
            self.wfile.write(message.encode())

        return

def startWeb():
    run (handler_class=HttpGetHandler)