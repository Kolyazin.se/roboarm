from tkinter import *
from tkinter import scrolledtext
import serial


def comPortOut():
    #b = int(spin1.get())
    sss = int(spin1.get())
    if sss > 100:
        sss = 100     
    #b = spin1.get().encode('ascii')
    #print(b)
    cmd = bytes(spin1.get(), 'ascii') 
    tmpBuffer = bytearray(sss)
    ser.write(cmd)
    logMes.insert(INSERT, "Tx=> " + str(cmd))
    logMes.insert(INSERT, '\n')


window = Tk() 
window.title("Servo CONTROL")
window.geometry('400x200')

spin1 = Spinbox(window, from_=0, to=100, width=5)
spin1.grid(column=0,row=0)

spin2 = Spinbox(window, from_=0, to=100, width=5)
spin2.grid(column=0,row=1)

spin3 = Spinbox(window, from_=0, to=100, width=5)
spin3.grid(column=0,row=2)

spin4 = Spinbox(window, from_=0, to=100, width=5)
spin4.grid(column=0,row=3)

spin5 = Spinbox(window, from_=0, to=100, width=5)
spin5.grid(column=0,row=4)

btn = Button(window, text="Установить", command=comPortOut)
btn.grid(column=1, row=2)

logMes = scrolledtext.ScrolledText(window, width=40, height=5)  
logMes.grid(column=0, row=5)
#logMes.insert(INSERT, 'Лог ==>')

ser = serial.Serial("COM4", 9600)

while(1):
    Tk.update(window)

    while ser.inWaiting() > 0:
        #out = ser.read(1)
        out = ser.readline()
        #print(out)
        logMes.insert(INSERT, "Rx=> " + str(out))
        logMes.insert(INSERT, '\n')
