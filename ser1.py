import serial
 
PORT = "/dev/ttyUSB0"
BAUD = 115200
TIMEOUT = 0.01    # Число секунд ожидания или None 
 
if __name__ == "__main__":
  # Пытаемся открыть последовательный порт
  try:
    ser = serial.Serial(PORT)
  except: # SerialException:
    print("Проблема: не могу открыть порт {0}".format(PORT))
    exit(0)
 
  # Настраиваем последовательный порт
  ser.baudrate = BAUD
  ser.timeout  = TIMEOUT
  ser.bytesize = serial.EIGHTBITS
  ser.parity   = serial.PARITY_NONE
  ser.stopbits = serial.STOPBITS_ONE
 
  pack = b""  # Изначально буфер пакета пустой
 
  while True:
    byte = ser.read() # Читаем один байт
    # print("{0:02X}".format(ord(byte))) # Строка для отладки
 
    if len(byte) != 0:
      # Принят байт
      pack += byte  # Добавляем принятый байт в буфер пакета
    else:
      # Сюда попадаем по таймауту.
      if len(pack) > 0:
        # В буфере пакета есть информация, выведем её по-байтно в HEX-формате
        for b in pack:
          print("{0:02X}".format(b), end=" ") #// Печатаем байты в строку через пробел
        print()  #// Новая строка
 
        pack = b""  # Начинаем с начала
