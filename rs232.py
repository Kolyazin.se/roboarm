#!/usr/bin/env python

# based on tutorials:
#   http://www.roman10.net/serial-port-communication-in-python/
#   http://www.brettdangerfield.com/post/raspberrypi_tempature_monitor_project/

import serial
import time
from port import serial_ports,speeds

ser = None

def startSerial():
    global ser

    ports = serial_ports()
    if (len(ports) == 0):
        print("No serial ports found!")
        return
        
    print(ports)

    SERIALPORT = ports[0]
    BAUDRATE = 9600

    ser = serial.Serial(SERIALPORT, BAUDRATE)

    ser.bytesize = serial.EIGHTBITS #number of bits per bytes

    ser.parity = serial.PARITY_NONE #set parity check: no parity

    ser.stopbits = serial.STOPBITS_ONE #number of stop bits

    #ser.timeout = None          #block read

    #ser.timeout = 0             #non-block read

    ser.timeout = 2              #timeout block read

    ser.xonxoff = False     #disable software flow control

    ser.rtscts = False     #disable hardware (RTS/CTS) flow control

    ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control

    ser.writeTimeout = 0     #timeout for write

    print ('Starting Up Serial Monitor')

    #try:
    #    ser.open()
    #
    #except:
    #    print ("error open serial port")
    #    exit()

    if ser.isOpen():

        ser.flushInput() #flush input buffer, discarding all its contents
        ser.flushOutput()#flush output buffer, aborting current output

        ser.write(b"s1=10\r\n")
        print("write data: ATI")
        time.sleep(0.5)
        numberOfLine = 0

        while True:
    #        ser.write(b"s1=10\r\n")
            response = ser.readline()
            if len(response) != 0:
                print("read data: " + str(response))
    #        response = ser.read()
    #        print(str(response))


    #        numberOfLine = numberOfLine + 1
    #        if (numberOfLine >= 50):
    #            break

        ser.close()

    else:
        print ("cannot open serial port")
    return


def sendToSerial(cmd):
    global ser
    ser.write(bytes(cmd+"\r\n", encoding="ascii"))
    return
